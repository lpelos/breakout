// Keyboard Class ********************************************************
function Keyboard () {
  var self = this;
  var directionKeys = {
    up: 38,
    down: 40,
    left: 37,
    right: 39
  };

  this.directionKeysState = {
    up: false,
    down: false,
    left: false,
    right: false
  }

  this.isKeyPressed = function (key) {
    return this.directionKeysState[key];
  }

  this.setDirectionKey = function (pressState, keyCode) {
    for (key in directionKeys) {
      if (keyCode == directionKeys[key]) {
        if (pressState == "down") {
          self.directionKeysState[key] = true;
        } else if (pressState == "up") {
          self.directionKeysState[key] = false;
        }
        break;
      }
    }
  }

  this.startListening = function () {
    addEventListener("keydown", function (e) {
      self.setDirectionKey("down", e.keyCode)
    }, false);

    addEventListener("keyup", function (e) {
      self.setDirectionKey("up", e.keyCode)
    }, false);
  }

  return this;
}

// Vector Class ********************************************************
function Vector (x, y) {
  this.x = x;
  this.y = y;

  this.add = function (vector) {
    this.x += vector.x;
    this.y += vector.y;
  }

  return this;
}

// Bar Class ********************************************************
function PlayerBar () {
  this.height = 10;
  this.width = 200;
  this.position = new Vector(canvas.width/2 - this.width/2, 550);
  this.velocity = 10;

  this.update = function() {
    if (kb.isKeyPressed("left")) this.position.x -= this.velocity;
    if (kb.isKeyPressed("right")) this.position.x += this.velocity;
  }

  this.checkbounds = function(canvas) {
    if (this.position.x < 0) {
      this.position.x = 0
    } else if (this.position.x + this.width > canvas.width) {
      this.position.x = canvas.width - this.width
    }
  }

  this.draw = function(canvas, ctx) {
    ctx.fillStyle = "#FF0000";
    ctx.fillRect(this.position.x, this.position.y, this.width, this.height);
  }

  return this;
}

// Ball Class ********************************************************
function Ball (x, y) {
  this.position = new Vector(x, y);
  this.velocity = new Vector(5, 2.5);
  this.radius = 10;

  this.update = function() {
    this.position.add(this.velocity);
  }

  this.checkCollisionWith = function(object) {
    // Collision from above
    if (this.position.y + this.radius >= object.position.y &&
      this.position.y + this.radius < object.position.y + object.height &&
      this.position.x >= object.position.x &&
      this.position.x <= object.position.x + object.width
    ){
      this.position.y = object.position.y - this.radius;
      this.velocity.y *= -1;
      return true;
    }

    // Collision from below
    if (this.position.y - this.radius <= object.position.y + object.height &&
      this.position.y - this.radius > object.position.y &&
      this.position.x >= object.position.x &&
      this.position.x <= object.position.x + object.width
    ){
      this.position.y = object.position.y + object.height + this.radius;
      this.velocity.y *= -1;
      return true;
    }

    // Collision from left side
    if (this.position.x + this.radius >= object.position.x &&
      this.position.x + this.radius < object.position.x + object.width &&
      this.position.y >= object.position.y &&
      this.position.y <= object.position.y + object.height
    ){
      this.position.x = object.position.x - this.radius;
      this.velocity.x *= -1;
      return true;
    }

    // Collision from right side
    if (this.position.x - this.radius <= object.position.x + object.width &&
      this.position.x - this.radius > object.position.x &&
      this.position.y >= object.position.y &&
      this.position.y <= object.position.y + object.height
    ){
      this.position.x = object.position.x + object.width + this.radius;
      this.velocity.x *= -1;
      return true;
    }

  }

  this.checkbounds = function(area) {
    var radiusPlusBorder = this.radius + 1;
    if (this.position.x > area.width  - radiusPlusBorder || this.position.x < 0 + radiusPlusBorder)
      this.velocity.x *= -1

    if (this.position.y > area.height - radiusPlusBorder || this.position.y < 0 + radiusPlusBorder)
      this.velocity.y *= -1
  }

  this.draw = function(canvas, ctx) {
    ctx.beginPath();
    ctx.arc(this.position.x, this.position.y, this.radius, 0, 2*Math.PI, false);
    ctx.fillStyle = "green";
    ctx.strokeStyle = "black";
    ctx.lineWidth = 1;
    ctx.fill();
    ctx.stroke();
  }

  return this;
}

// Square Class ********************************************************
function Square (x, y) {
  this.position = new Vector(x, y);
  this.height = 40;
  this.width = 40;

  this.draw = function(canvas, ctx) {
    ctx.fillStyle = "#0000ff";
    ctx.strokeStyle = "black";
    ctx.lineWidth = 1;
    ctx.fillRect(this.position.x, this.position.y, this.width, this.height);
    ctx.strokeRect(this.position.x, this.position.y, this.width, this.height);
  }

  return this;
}


var canvas, ctx, playerBar, ball, squares;
var kb = new Keyboard();

function init() {
  canvas = document.getElementById("canvas");
  ctx = canvas.getContext("2d");
  kb.startListening();
  ball = new Ball(canvas.width / 2, canvas.height / 2);
  playerBar = new PlayerBar();
  squares = [];

  for (var i = 20; i <= canvas.width - 60; i += 40) {
    squares.push([]);
    for (var j = 20; j <= 200; j += 40) {
      squares[squares.length - 1].push(new Square(i, j));
    }
  }
}

function animate() {
  requestAnimationFrame(animate);
  draw();
}

function draw() {
  ctx.clearRect(0, 0, canvas.width, canvas.height);

  playerBar.update();
  ball.update();

  playerBar.checkbounds(canvas);
  ball.checkCollisionWith(playerBar);

  for (var i = 0; i < squares.length; i++) {
    for (var j = 0; j < squares[i].length; j++) {
      if (squares[i][j] instanceof Square)
        if (ball.checkCollisionWith(squares[i][j]))
          delete squares[i][j];
    }
  }

  ball.checkbounds(canvas);

  playerBar.draw(canvas, ctx);
  ball.draw(canvas, ctx);
  for (var i = 0; i < squares.length; i++) {
    for (var j = 0; j < squares[i].length; j++) {
      if (squares[i][j] instanceof Square)
        squares[i][j].draw(canvas, ctx);
    }
  }

}

jQuery(function($){
  init();
  animate(ball);
});
